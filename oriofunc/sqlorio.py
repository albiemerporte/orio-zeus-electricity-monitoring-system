
import sqlite3

def save_overload(*oload):
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("insert into loadtbl(Load_Name, Notification_Message, Wattage_Of_Load, Date_Added, Overload, Date_Seen) values(?,?,?,?,?,?)", \
                  (oload[0], oload[1], oload[2], oload[3], oload[4], oload[5]))
        conn.commit()

#print(date.today())
        
def sqlcountnotif():
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("select count(id) from loadtbl")
        rows = c.fetchall()
        conn.rollback()
    return rows

#print(sqlreadallnotif())

def sqlreadallnotif():
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("select * from loadtbl ORDER BY id DESC")
        rows = c.fetchall()
        conn.rollback()
    return rows

def sqlsavenewwattage(newwattage):
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("update wattagelimitertbl set Maximum_Wattage = ?, Safety_Factor = ?, Kilowatts_Per_Hour_Rate = ?", \
                  (newwattage[0], newwattage[1], newwattage[2]))
        
        conn.commit()

def sqlsavenewamperage(newamperage):
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("update amperagelimitertbl set Amperage_Load1 = ?, Amperage_Load2 = ?,Amperage_Load3 = ?", \
                  (newamperage[0], newamperage[1], newamperage[2]))
        
        conn.commit()
        
def sqlreadallampere():
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("select * from amperagelimitertbl")
        rows = c.fetchone()
        conn.rollback()
    return rows

def sqlreadallwattage():
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("select * from wattagelimitertbl")
        rows = c.fetchone()
        conn.rollback()
    return rows

def sqlmetering(*newmeter):
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("update newupdatecosttbl set Total_Kilowatts_Usage = ?, Approximate_Billing_Cost = ?, Date = ?", \
                  (newmeter[0], newmeter[1], newmeter[2]))
        
        conn.commit()

def sqlsavenewkwph(newwattage):
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("update newupdatecosttbl set Kilowatts_Per_Hour = ?", \
                  (newwattage,))
        
        conn.commit()

def sqlselectupdatemetering():
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("select * from newupdatecosttbl")
        rows = c.fetchone()
        conn.rollback()
    return rows

"""def sqlkwhread():
    with sqlite3.connect('db/oriodb.db') as conn:
        c = conn.cursor()
        c.execute("select * from wattagelimitertbl")
        rows = c.fetchone()
        conn.rollback()
    return rows"""