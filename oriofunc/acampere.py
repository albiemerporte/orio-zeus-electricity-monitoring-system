def getamperage(self):
    self.mainui.lblMaxAmpere.setText("{0}".format(self.ac_current))
        
    self.mainui.lblCUA.setText("{0}".format(self.roomoutlet.outlet_total_ampere()))
        
    available_ampere = round(self.ac_current - self.roomoutlet.outlet_total_ampere(), 2)
    self.mainui.lblAvailableAmpere.setText("{0}".format(available_ampere))
