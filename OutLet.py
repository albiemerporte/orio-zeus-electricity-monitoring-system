
import random
from mypyserial import CurrentLoad1
#from oriofunc import

class RoomOutLet:
    """ This is for puring sensor from arduino"""
    def __init__(self):
        self.c_value_kitchen = CurrentLoad1().current_load1() #random.uniform(0, 15) #  # 1.67  # from current sensor formula c_value = input_apparent-power / 230
        self.v_value_kitchen = CurrentLoad1().voltage_load1() #random.uniform(0, 240) # 9.0  # from voltage sensor

        self.c_value_bed = random.uniform(0,15)
        self.v_value_bed = random.uniform(0,240)

        self.c_value_living = random.uniform(0, 15)
        self.v_value_living = random.uniform(0, 240)

    def kitchen_room_outlet(self):
        # kitchen outlet sensor
        return self.c_value_kitchen * self.v_value_kitchen
        

    def bed_room_outlet(self):
        return self.c_value_bed * self.v_value_bed

    def living_room_outlet(self):
        return self.c_value_living * self.v_value_living
    
    def outlet_total_ampere(self):
        return round(sum([self.c_value_kitchen, self.c_value_bed, self.c_value_living]), 1)
    
    def outlet_average_voltage(self):
        return round(sum([self.v_value_kitchen, self.v_value_bed, self.v_value_living]) / 3, 2)
 
    def outlet_overall_voltage(self):
        if self.v_value_kitchen > self.v_value_bed and self.v_value_kitchen > self.v_value_living:
            return round(self.v_value_kitchen, 2)
        if self.v_value_bed > self.v_value_kitchen and self.v_value_bed > self.v_value_living:
            return round(self.v_value_bed, 2)
        if self.v_value_living > self.v_value_kitchen and self.v_value_living > self.v_value_bed:
            return round(self.v_value_living)
        return None
    
    def totalsumwattcostfunc(self): # to repair when have a sensor
        ## get previous watts from sqldb
        self.totalwattsforcost = sum([self.kitchen_room_outlet(), self.bed_room_outlet(), self.living_room_outlet()])
        
        return self.totalwattsforcost
    
    def totalkilowattscostfunc(self): # to repair when have a sensor
        totalpowerconsumption = self.totalsumwattcostfunc() / 1000
        return totalpowerconsumption

#while True:
    #print(CurrentLoad1().current_load1(), CurrentLoad1().voltage_load1())
"""while True:
    print(RoomOutLet().kitchen_room_outlet())
    print(RoomOutLet().bed_room_outlet())
    print(RoomOutLet().living_room_outlet())"""
#print("Total used watts: ", RoomOutLet().totalsumwattcostfunc())
#print("Total Cost Amount: ", RoomOutLet().totalkilowattscostfunc())
#print(RoomOutLet().outlet_average_voltage())