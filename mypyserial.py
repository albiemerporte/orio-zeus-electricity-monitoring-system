
import serial
import time

arduino_port = '/dev/ttyUSB0'
baud_rate = 230400

class PowerRelay:
    def __init__(self):
        
        import serial
        
        self.ser = serial.Serial(arduino_port, baud_rate)
        
        time.sleep(2)
        
    def turn_on_relay(self):
        self.ser.write(b'ON\n')
        print("Relay is Turned ON")
        
    def turn_off_relay(self):
        self.ser.write(b'OFF\n')
        print("Relay is Turned OFF")

class CurrentLoad1:
    def __init__(self):    
        
        self.ser = serial.Serial(arduino_port, baud_rate)
        
        
        try:
            data = self.read_data()
            self.current, self.voltage = map(float, data.split(','))
        
            #voltage_from_sensor = (voltage_sensor / 1023.0) * reference_voltage
        
            #actual_voltage = (voltage_from_sensor / reference_voltage) * max_voltage
        
            #if current < 0:
             #   current = 0
        
            #print("Current: ", self.current_load1())
            #print("Voltage: ", self.voltage_load1())
            #print("watts: ", self.current_load1() * self.voltage_load1())
        except KeyboardInterrupt:
            print("program terminated")
    
        finally:
            self.ser.close()

    def current_load1(self):
        return self.current
    
    def voltage_load1(self):
        return self.voltage

    def read_data(self):
        line = self.ser.readline().decode('utf-8').strip()
        return line
    
"""if __name__ == '__main__':
    while True:
        load1 = CurrentLoad1()"""