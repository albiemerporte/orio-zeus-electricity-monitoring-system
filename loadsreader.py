
################## INPUT WATTAGE LOADS INTO DC ############################
#sample loads charger
# this is input power into charger from AC
# the c_value will be depends if theres a cellphone charging and
# not totally use the maximize of 0.5
c_value = 2.0       # 0.5 ampere
v_value = 220      # 220 voltage of ac

load_wattage = c_value * v_value

print("The Input Wattage of loads is: ", load_wattage)

########### APPARENT POWER OF LOADS (S) ######################

ac_voltage = v_value
ac_current = c_value                # if household measurement it should be 30Amp
loads_va = ac_current * ac_voltage
s_apparent_power_loads = loads_va

print("Apparent Power of loads is: ", s_apparent_power_loads)

################### POWER FACTOR OF LOADS (PF)###########################

p_factor_loads = load_wattage / s_apparent_power_loads

print("Power Factor of loads is: ", p_factor_loads)

if p_factor_loads >= 0.8 or p_factor_loads == 1:
    print("Resistive Loads")
elif p_factor_loads >= 0.5:
    print("Capacitive Loads")
elif p_factor_loads >= 0.01:
    print("Inductive Loads")
else:
    print("No Loads Detected")

################## REAL POWER OF LOADS ############################

p_real_power = s_apparent_power_loads * p_factor_loads

print("Real Power loads is: ", p_real_power)