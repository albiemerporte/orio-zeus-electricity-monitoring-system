def getvoltage(self):
    self.mainui.lblHouseVoltage.setText("{0}".format(self.ac_voltage))
        
    self.mainui.lblOUV.setText("{0}".format(self.roomoutlet.outlet_overall_voltage()))
        
    self.mainui.lblAUV.setText("{0}".format(self.roomoutlet.outlet_average_voltage()))