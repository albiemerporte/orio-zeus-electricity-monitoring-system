

########################### CIRCUIT APPARENT POWER ####################

ac_current = 30
ac_voltage = 220

ac_apparent_power = ac_current * ac_voltage

print("AC APPARENT POWER: ",  ac_apparent_power)



########################### INPUT AC WATTAGE INTO LOADS ####################


in_current = 1.67
in_voltage = 9.0
input_real_power = in_current * in_voltage

print("INPUT REAL POWER INTO LOAD: ", input_real_power)


########################### POWER FACTOR OF LOADS ####################

input_real_power = 15.3
ac_apparrent_power = 6600

p_factor_loads = input_real_power / ac_apparent_power

print("LOAD POWER FACTOR: ", p_factor_loads)


########################### PERCENTAGE REAL POWER OF LOADS ####################

input_real_power = 15.3
ac_apparent_power = 6600
percentage = (input_real_power / ac_apparent_power) * 100

print("PERCENTAGE USED REAL POWER BY LOAD: ", round(percentage, 2),"%")

########################### PERCENTAGE REAL POWER OF LOADS ####################

