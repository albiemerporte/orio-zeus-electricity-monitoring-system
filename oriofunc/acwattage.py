
from .sqlorio import save_overload, sqlreadallwattage, sqlreadallampere

import pygame
import os
import sys

from datetime import date

sys.stdout = open(os.devnull, 'w')

# Initialize pygame
pygame.init()

# Restore stdout if you want to print anything later
sys.stdout = sys.__stdout__

loadname = ""
overload_state = False
try:
    pygame.mixer.init()
except pygame.error as e:
    #print(f"Audio initialization failed: {e}. Continuing without sound.")
    # Optionally, you can set a flag to disable sound-related features in your program
    sound_enabled = False
else:
    sound_enabled = True

def acwattagedata(self, thetime):
    self.mainui.lblMaxWattage.setText("{0}".format( sqlreadallwattage()[1] ))
    #print("the max wattage: ", sum(sqlreadallampere()) * 240)
        #self.mainui.lblOUW.setText("  Overall Used Wattage: {0}".format())
    ouw = self.outlet1[len(self.outlet1) - 1] + \
          self.outlet2[len(self.outlet2) - 1] + \
          self.outlet3[len(self.outlet3) - 1]
    
    self.mainui.lblWSF.setText("{0}".format(sqlreadallwattage()[2])) #self.ac_apparent_power[0] * 0.80))
    
    if ouw >= self.limiter[0]:
        self.mainui.lblOUW.setStyleSheet("color: rgb(255, 0, 0);")
    else:
        self.mainui.lblOUW.setStyleSheet("color: rgb(255, 255, 255);")
    self.mainui.lblOUW.setText("{0}".format(round(ouw,1)))

    alarm1_file_path = os.path.join(os.path.dirname(__file__), "..", "resources", "kitchen_room.mp3")
    alarm2_file_path = os.path.join(os.path.dirname(__file__), "..", "resources", "bed_room.mp3")
    alarm3_file_path = os.path.join(os.path.dirname(__file__), "..", "resources", "living_room.mp3")
    alarmsf_file_path = os.path.join(os.path.dirname(__file__), "..", "resources", "overload.mp3")

    values = {
        "kitchen": self.outlet1[len(self.outlet1) - 1],
        "bed": self.outlet2[len(self.outlet2) - 1],
        "living": self.outlet3[len(self.outlet3) - 1]
    }
    
    
    totaallocate = 0 #sum([values["kitchen"], values["bed"], values["living"]])
    
    basedallocation = [values["kitchen"], values["bed"], values["living"]]
    
    activeloads = sum([values["kitchen"] > 0, values["bed"] > 0, values["living"] > 0])
    
    if self.outlet1[len(self.outlet1) - 1] < 1 or self.outlet2[len(self.outlet2) - 1] < 1 or \
       self.outlet3[len(self.outlet3) - 1] < 1:
        #highest_loads = max(values, key=values.get) put a try if implemented
        
        highestloads = max(basedallocation)
        
        #print("The Highest loads: ", round(highestloads))
        #print("The active loads: ", activeloads)
        
        
        #print("Initial Allocate: ", round(totaallocate))
    
    else:
        
        highestloads = 0
        lowestloads = 0
        
        #print("no allocation occur")
        #print("no allocation occur")
        
    
    if ouw >= sqlreadallwattage()[2]:
        self.mainui.lblDashboardMessage.setText(f" Exceeded from the Level of Safety Factor at the Time of {thetime}")
        
        pygame.mixer.music.load(alarmsf_file_path)
        pygame.mixer.music.play()
        
    else:
        outletlimiter1 = 0
        outletlimiter2 = 0
        outletlimiter3 = 0
        
        # this is a port for ampere limiter
        if activeloads == 2:
            totaallocate = 4800
        elif activeloads < 2:
            totaallocate = 5760
        else:
            totaallocate = 2400
        
        #print("Total Allocated: ", round(totaallocate))
        
        """if self.outlet1[len(self.outlet1) - 1] == highestloads:
            outletlimiter1 = round(totaallocate)
            print("high loads 1 ok")
        elif self.outlet2[len(self.outlet2) - 1] == highestloads:
            outletlimiter2 = round(totaallocate)
            print("high loads 2 ok")
        elif self.outlet3[len(self.outlet3) - 1] == highestloads:
            outletlimiter3 = round(totaallocate)
            print("high loads 3 ok")"""
     
        if self.outlet1[len(self.outlet1) - 1] > round(totaallocate):
            #print("Wattage")
            notifmsg = f"High Load in Kitchen Room Outlet at the Time of {thetime}"
            self.mainui.lblDashboardMessage.setText(notifmsg)
            loadname = "Outlet 1"
            overload_state = True
            date_seen = ""
            
            pygame.mixer.music.load(alarm1_file_path)
            pygame.mixer.music.play()
            
            #print("outletlimiter1: ", outletlimiter1)
            
            save_overload(loadname, notifmsg, round(self.outlet1[len(self.outlet1) - 1]), date.today(), overload_state, date_seen)

        if self.outlet2[len(self.outlet2) - 1] > round(totaallocate):
            #print("Wattage")
            notifmsg = f"High Load in Bed Room Outlet at the Time of {thetime}"
            self.mainui.lblDashboardMessage.setText(notifmsg)
            loadname = "Outlet 2"
            overload_state = True
            date_seen = ""

            pygame.mixer.music.load(alarm2_file_path)
            pygame.mixer.music.play()

            #print("outletlimiter2: ", outletlimiter2)

            save_overload(loadname, notifmsg, round(self.outlet2[len(self.outlet2) - 1]), date.today(), overload_state, date_seen)

        if self.outlet3[len(self.outlet3) - 1] > round(totaallocate):
            #print("Wattage")
            notifmsg = f"High Load in Living Room Outlet at the Time of {thetime}"
            self.mainui.lblDashboardMessage.setText(notifmsg)
            loadname = "Outlet 3"
            overload_state = True
            date_seen = ""
            
            pygame.mixer.music.load(alarm3_file_path)
            pygame.mixer.music.play()
            
            #print("outletlimiter2: ", outletlimiter3)
            
            save_overload(loadname, notifmsg, round(self.outlet3[len(self.outlet3) - 1]), date.today(), overload_state, date_seen)
        
    #print(wattamperelimiter)
    #print("ouw: ", ouw)
    #print("safety factor: ", self.ac_apparent_power[0] * 0.80)
    #print("current: ", self.ac_current)
    #print("outlet: ", self.roomoutlet.outlet_total_ampere())
    ##print("1:", round(self.outlet1[len(self.outlet1) - 1]))
    ##print("2:", round(self.outlet2[len(self.outlet2) - 1]))
    ##print("3:", round(self.outlet3[len(self.outlet3) - 1]))
    #print("load1 :", self.outlet1)