#include <ZMPT101B.h>
#include <ACS712.h>

ACS712 sensor(ACS712_30A, A0);
ZMPT101B voltageSensor(A1, 50.0);

void setup(){
  Serial.begin(230400);
  sensor.calibrate();
  voltageSensor.setSensitivity(500.0f);
  }

void loop(){
  float I = sensor.getCurrentAC();
  float voltage = voltageSensor.getRmsVoltage();
  
  Serial.print(I);
  Serial.print(",");
  Serial.println(voltage);

 delay(20000);
} // void loop end
