
import RPi.GPIO as GPIO

from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtCore import QTimer, Qt

from PyQt6.QtGui import QMovie

from oriofunc.acwattage import acwattagedata
from oriofunc.acampere import getamperage
from oriofunc.acvoltage import getvoltage
from OutLet import RoomOutLet

from oriofunc import sqlcountnotif, sqlreadallnotif, sqlsavenewwattage, sqlsavenewamperage, \
     sqlreadallwattage, sqlreadallampere, sqlmetering, sqlsavenewkwph, sqlselectupdatemetering

import resources     #ignore error says by ide
import pyqtgraph as pg
import os
import sys
import time

from datetime import datetime, date

class Graph:
    def __init__(self):
        
        self.roomoutlet = RoomOutLet()
        
        self.splash = loadUi("splash.ui")
        self.splash.showFullScreen()
        
        self.movie = QMovie("resources/oriozeus.gif")  # Replace with your GIF file path
        self.splash.lblsplash.setMovie(self.movie)
        self.movie.start()
        
        self.mainui = loadUi('Dashboard.ui')
        
        QTimer.singleShot(10000, self.splashfunc)
        
        self.plot_widget = pg.PlotWidget()
        self.mainui.qvboxPf.addWidget(self.plot_widget)

        self.mainui.pbWattage.clicked.connect(self.tabwattage)
        self.mainui.pbAmperage.clicked.connect(self.tabamperage)
        self.mainui.pbCost.clicked.connect(self.tabcost)
        
        self.mainui.pbShutdown.clicked.connect(self.shutdownfunc)

        self.mainui.pbMenu.clicked.connect(self.showmenu)
        self.burgermenuvisible = False 

        self.mainui.fmeMenu.hide()
        self.mainui.tabElectricity.tabBar().setVisible(False)

        self.mainui.chkKitchen.stateChanged.connect(self.outlet1func)
        self.mainui.chkBed.stateChanged.connect(self.outlet2func)
        self.mainui.chkLiving.stateChanged.connect(self.outlet3func)

        self.mainui.pbNotification.clicked.connect(self.tabnotification)
        self.mainui.pbDashboard.clicked.connect(self.tabdashboard)

        self.initassignment()

        self.timer = QTimer()
        self.timer.timeout.connect(self.plot_chart)
        self.timer.start(30000)  # 30 seconds in milliseconds
        
        #self.plot_chart() no longer need only for test
        
        self.notificationdesign()
        self.notificationupdatelist()
        self.notifcount = 0
        
        self.wattagesliderprecondition()
        self.amperesliderprecondition()
        
        self.mainui.pbSaveWattageLimiter.clicked.connect(self.savenewwattageset)
        self.mainui.pbMWMinus.clicked.connect(self.pbmwminusfunc)
        self.mainui.pbMWPlus.clicked.connect(self.pbmwplusfunc)
        self.mainui.pbSFMinus.clicked.connect(self.pbsfminusfunc)
        self.mainui.pbSFPlus.clicked.connect(self.pbsfplusfun)
        self.mainui.pbKWPHRMinus.clicked.connect(self.pbkwphrminusfunc)
        self.mainui.pbKWPHRPlus.clicked.connect(self.pbkwphrplusfunc)

        self.mainui.pbL1ALMinus.clicked.connect(self.pbl1alminus)
        self.mainui.pbL1ALPlus.clicked.connect(self.pbl1alplus)
        self.mainui.pbL2ALMinus.clicked.connect(self.pbl2alminus)
        self.mainui.pbL2ALPlus.clicked.connect(self.pbl2alplus)
        self.mainui.pbL3ALMinus.clicked.connect(self.pbl3alminus)
        self.mainui.pbL3ALPlus.clicked.connect(self.pbl3alplus)

        self.mainui.pbSaveLoadAmpLimiter.clicked.connect(self.savenewamperageset)
        
        self.postampereoutput()
        self.postwattageoutput()
        
        self.kilo_watts_per_hour_meter()
        self.displaylabeltotalconsumption()

        self.relay_pin = 18
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.relay_pin, GPIO.OUT)
        GPIO.output(self.relay_pin, GPIO.HIGH)
        self.mainui.chkKitchen.stateChanged.connect(self.relaypowershit)

        
    def relaypowershit(self, state):
        self.restartserialread()
        if state == 2:
            GPIO.output(self.relay_pin, GPIO.HIGH)
        else:
            GPIO.output(self.relay_pin, GPIO.LOW)
        
            
        #if self.mainui.chkBed.isChecked():
            
        #if self.mainui.chkLiving.isChecked():
           
    
    def restartserialread(self):
        self.timer.stop()
        self.timer.start(30000)
        print("Timer restarted!")
    
    def pbl3alplus(self):
        self.restartserialread()
        self.mainui.pbL3ALMinus.setDisabled(False)
        load3 = int(self.mainui.lblLoad3AmpData.text())
        if load3 >= 0 and load3 < 30:
            load3 += 1
        elif load3 == 30:
            load3 = 30
        self.mainui.lblLoad3AmpData.setText(str(load3))
        self.mainui.hsliderLoad3AmpLimiter.setValue(int(load3))
        self.mainui.hsliderLoad3AmpLimiter.repaint()
    
    def pbl3alminus(self):
        self.restartserialread()
        load3 = int(self.mainui.lblLoad3AmpData.text())
        if load3 >= 1:
            load3 -= 1
        elif load3 == 0 and load3 < 1:
            self.mainui.pbL3ALMinus.setDisabled(True)
        self.mainui.lblLoad3AmpData.setText(str(load3))
        self.mainui.hsliderLoad3AmpLimiter.setValue(int(load3))
        self.mainui.hsliderLoad3AmpLimiter.repaint()
    
    def pbl2alplus(self):
        self.restartserialread()
        self.mainui.pbL2ALMinus.setDisabled(False)
        load2 = int(self.mainui.lblLoad2AmpData.text())
        if load2 >= 0 and load2 < 30:
            load2 += 1
        elif load2 == 30:
            load2 = 30
        self.mainui.lblLoad2AmpData.setText(str(load2))
        self.mainui.hsliderLoad2AmpLimiter.setValue(int(load2))
        self.mainui.hsliderLoad2AmpLimiter.repaint()
    
    def pbl2alminus(self):
        self.restartserialread()
        load2 = int(self.mainui.lblLoad2AmpData.text())
        if load2 >= 1:
            load2 -= 1
        elif load2 == 0 and load2 < 1:
            self.mainui.pbL2ALMinus.setDisabled(True)
        self.mainui.lblLoad2AmpData.setText(str(load2))
        self.mainui.hsliderLoad2AmpLimiter.setValue(int(load2))
        self.mainui.hsliderLoad2AmpLimiter.repaint()
    
    def pbl1alplus(self):
        self.restartserialread()
        self.mainui.pbL1ALMinus.setDisabled(False)
        load1 = int(self.mainui.lblLoad1AmpData.text())
        if load1 >= 0 and load1 < 30:
            load1 += 1
        elif load1 == 30:
            load1 = 30
        self.mainui.lblLoad1AmpData.setText(str(load1))
        self.mainui.hsliderLoad1AmpLimiter.setValue(int(load1))
        self.mainui.hsliderLoad1AmpLimiter.repaint()
    
    def pbl1alminus(self):
        self.restartserialread()
        load1 = int(self.mainui.lblLoad1AmpData.text())
        if load1 >= 1:
            load1 -= 1
        elif load1 == 0 and load1 < 1:
            self.mainui.pbL1ALMinus.setDisabled(True)
        self.mainui.lblLoad1AmpData.setText(str(load1))
        self.mainui.hsliderLoad1AmpLimiter.setValue(int(load1))
        self.mainui.hsliderLoad1AmpLimiter.repaint()
    
    def pbkwphrplusfunc(self):
        self.restartserialread()
        self.mainui.pbKWPHRMinus.setDisabled(False)
        kwph = int(self.mainui.lblKWPHRData.text())
        if kwph >= 0 and kwph < 100:
            kwph += 1
        elif kwph == 100:
            kwph = 100
        self.mainui.lblKWPHRData.setText(str(kwph))
        self.mainui.hsliderKWPHR.setValue(int(kwph))
        self.mainui.hsliderKWPHR.repaint()
    
    def pbkwphrminusfunc(self):
        self.restartserialread()
        kwph = int(self.mainui.lblKWPHRData.text())
        if kwph >= 1:
            kwph -= 1
        elif kwph == 0 and kwph < 1:
            self.mainui.pbKWPHRMinus.setDisabled(True)
        self.mainui.lblKWPHRData.setText(str(kwph))
        self.mainui.hsliderKWPHR.setValue(int(kwph))
        self.mainui.hsliderKWPHR.repaint()
    
    def pbsfplusfun(self):
        self.restartserialread()
        self.mainui.pbSFMinus.setDisabled(False)
        sf = int(self.mainui.lblSafetyFactorData.text())
        if sf >= 0 and sf < self.limiter[0]:
            sf += 1
            print(self.limiter[0])
        elif sf == self.limiter[0]:
            sf = self.limiter[0]
        self.mainui.lblSafetyFactorData.setText(str(round(sf)))
        self.mainui.hsliderSafetyFactor.setValue(int(sf))
        self.mainui.hsliderSafetyFactor.repaint()
    
    def pbsfminusfunc(self):
        self.restartserialread()
        sf = int(self.mainui.lblSafetyFactorData.text())
        if sf >= 1:
            sf -= 1
        elif sf == 0 and sf < 1:
            self.mainui.pbSFMinus.setDisabled(True)
        self.mainui.lblSafetyFactorData.setText(str(round(sf)))
        self.mainui.hsliderSafetyFactor.setValue(int(sf))
        self.mainui.hsliderSafetyFactor.repaint()
    
    def pbmwplusfunc(self):
        self.restartserialread()
        self.mainui.pbMWMinus.setDisabled(False)
        maxwatt = int(self.mainui.lblMaxWattageData.text())
        if maxwatt >= 0 and maxwatt <self.ac_apparent_power[0]:
            maxwatt += 1
            print(type(self.ac_apparent_power[0]))
        elif maxwatt == self.ac_apparent_power[0]:
            maxwatt = self.ac_apparent_power[0]
        self.mainui.lblMaxWattageData.setText(str(maxwatt))
        self.mainui.hsliderMaximumWattage.setValue(int(maxwatt))
        self.mainui.hsliderMaximumWattage.repaint()
    
    def pbmwminusfunc(self):
        self.restartserialread()
        maxwatt = int(self.mainui.lblMaxWattageData.text())
        if maxwatt >= 1:
            maxwatt -= 1
        elif maxwatt == 0 and maxwatt < 1:
            self.mainui.pbMWMinus.setDisabled(True)
        self.mainui.lblMaxWattageData.setText(str(maxwatt))
        self.mainui.hsliderMaximumWattage.setValue(int(maxwatt))
        self.mainui.hsliderMaximumWattage.repaint()
    
    def splashfunc(self):
        self.mainui.showFullScreen()
        time.sleep(10)
        self.splash.close()
    
    def kilo_watts_per_hour_meter(self):
        self.roomoutlet = RoomOutLet()
        #print("chupapi: ", sqlselectupdatemetering()[1])
        self.recordperseconds = 30 / 3600
        #newtotalsumwatts = sum([self.roomoutlet.totalsumwattcostfunc(), sqlselectupdatemetering()[1]])
        #savekilowatts = self.roomoutlet.totalkilowattscostfunc() * self.recordperseconds #sqlselectupdatemetering()[2]
        newkilowattshour = self.roomoutlet.totalkilowattscostfunc() * self.recordperseconds
        newtotalsumwatts = sum([newkilowattshour, sqlselectupdatemetering()[1]])
        totalcost = newtotalsumwatts * 11
        
        #print("New total sum watts: ", newtotalsumwatts)
        #print("New total kiowatts: ", self.roomoutlet.totalkilowattscostfunc())
        #print("New kilowatts Per Hour: ", sqlselectupdatemetering()[2])
        sqlmetering(newtotalsumwatts, totalcost, date.today())
    
    def displaylabeltotalconsumption(self):
        self.mainui.lblKphData.setText(str(sqlselectupdatemetering()[2]))
        self.mainui.lblTWCData.setText(str( round(sqlselectupdatemetering()[1], 3) ))
        self.mainui.lblApproxCostData.setText(str( round(sqlselectupdatemetering()[3], 3) ))
    
    def shutdownfunc(self):
        self.mainui.close()
        os.system("sudo shutdown now")
    
    def initassignment(self):
         #AC circuit
        #print("sum of sqlampere: ", sum(sqlreadallampere()))
        self.ac_current = sum([sqlreadallampere()[1], sqlreadallampere()[2], sqlreadallampere()[3]]) #30  # fix ac
        self.ac_voltage = 230  # 230  # fix ac
        self.ac_apparent_power = [self.ac_current * self.ac_voltage]

        #print("AC Apparent Power of Circuit: ", self.ac_apparent_power[0])

        self.outlet1 = [0]
        self.outlet2 = [0]
        self.outlet3 = [0]
        self.limiter = [self.ac_apparent_power[0] * 0.80]  #this is safety factor
        
        self.totalampere = [0] 

        #print("this is the limiter", self.limiter[0])
    
    def amperesliderprecondition(self):
        self.mainui.hsliderLoad1AmpLimiter.setRange(0,30)
        self.mainui.hsliderLoad2AmpLimiter.setRange(0,30)
        self.mainui.hsliderLoad3AmpLimiter.setRange(0,30)
        
        self.mainui.hsliderLoad1AmpLimiter.valueChanged.connect(self.amperesliderpostcondition)
        self.mainui.hsliderLoad2AmpLimiter.valueChanged.connect(self.amperesliderpostcondition)
        self.mainui.hsliderLoad3AmpLimiter.valueChanged.connect(self.amperesliderpostcondition)
    
    def amperesliderpostcondition(self):
        dataLoad1AmpLimiter = self.mainui.hsliderLoad1AmpLimiter.value()
        dataLoad2AmpLimiter = self.mainui.hsliderLoad2AmpLimiter.value()
        dataLoad3AmpLimiter = self.mainui.hsliderLoad3AmpLimiter.value()
        
        self.newamperagedata = (int(dataLoad1AmpLimiter), int(dataLoad2AmpLimiter), int(dataLoad3AmpLimiter))
        
        self.mainui.lblLoad1AmpData.setText(str(dataLoad1AmpLimiter))   #this is is slider number
        self.mainui.lblLoad2AmpData.setText(str(dataLoad2AmpLimiter))
        self.mainui.lblLoad3AmpData.setText(str(dataLoad3AmpLimiter))
        
        self.mainui.lblmessageamperage.setText(" ")
    
    def postampereoutput(self):
        self.mainui.lblCWSSData.setText(str(sqlreadallwattage()[1]))
        self.mainui.lblCASSData.setText(str( sum([sqlreadallampere()[1], sqlreadallampere()[2], sqlreadallampere()[3]]) ))
        self.mainui.lblCVSSData.setText(str(230))
    
    def savenewamperageset(self):
        self.restartserialread()
        sqlsavenewamperage(self.newamperagedata)
        self.mainui.lblmessageamperage.setText("New Amperage adjustment saved.")
        self.postampereoutput()
        self.postwattageoutput()
        self.initassignment()
        self.wattagesliderprecondition()
    
    def wattagesliderprecondition(self):
    
        self.mainui.hsliderMaximumWattage.setRange(0, self.ac_apparent_power[0])
        self.mainui.hsliderSafetyFactor.setRange(0, round(self.limiter[0]))
        self.mainui.hsliderKWPHR.setRange(0,100)
        
        self.mainui.hsliderMaximumWattage.valueChanged.connect(self.wattagesliderpostcondition)
        self.mainui.hsliderSafetyFactor.valueChanged.connect(self.wattagesliderpostcondition)
        self.mainui.hsliderKWPHR.valueChanged.connect(self.wattagesliderpostcondition)
    
    def wattagesliderpostcondition(self):
        
        datamaximumwattage = self.mainui.hsliderMaximumWattage.value()
        datasafetyfactor = self.mainui.hsliderSafetyFactor.value()
        datakwphr = self.mainui.hsliderKWPHR.value()

        self.newwattagedata = (int(datamaximumwattage), int(datasafetyfactor), int(datakwphr))
        
        self.mainui.lblMaxWattageData.setText(str(datamaximumwattage))  #This is the slider number dont confuse
        self.mainui.lblSafetyFactorData.setText(str(datasafetyfactor))
        self.mainui.lblKWPHRData.setText(str(datakwphr))
        
        self.mainui.lblWattageMessage.setText(" ")
    
    def postwattageoutput(self):
        self.mainui.lblMWSSData.setText(str(sqlreadallwattage()[1]))
        self.mainui.lblSWSSData.setText(str(sqlreadallwattage()[2]))
        self.mainui.lblKWPHSSData.setText(str(sqlreadallwattage()[3]))
    
    def savenewwattageset(self):
        self.restartserialread()
        #print("the new wattage: ", self.newwattagedata)
        sqlsavenewwattage(self.newwattagedata)
        sqlsavenewkwph(self.newwattagedata[2])
        self.mainui.lblWattageMessage.setText("New Wattage adjustment saved.")
        self.postampereoutput()
        self.postwattageoutput()
    
    def tabwattage(self):
        self.restartserialread()
        self.mainui.tabElectricity.setCurrentIndex(1)
        
    def tabamperage(self):
        self.restartserialread()
        self.mainui.tabElectricity.setCurrentIndex(2)
        
    def tabcost(self):
        self.restartserialread()
        self.mainui.tabElectricity.setCurrentIndex(3)
        self.displaylabeltotalconsumption()
    
    def tabdashboard(self):
        self.mainui.tabElectricity.setCurrentIndex(0)
        self.showmenu()
        self.restartserialread()
        
    def tabnotification(self):
        self.restartserialread()
        self.mainui.tabElectricity.setCurrentIndex(4)
        self.notificationupdatelist()
        self.mainui.fmeMenu.show()
        self.burgermenuvisible = True

    def notificationupdatelist(self):
        self.mainui.ListwNotification.clear()
        for row in sqlreadallnotif():
            if row[1] == "Outlet 1":
                self.mainui.ListwNotification.addItem(str(row[2]))
                        
            if row[1] == "Outlet 2":
                self.mainui.ListwNotification.addItem(str(row[2]))
                        
            if row[1] == "Outlet 3":
                self.mainui.ListwNotification.addItem(str(row[2]))
        

    def notificationdesign(self):
        
        if sqlcountnotif()[0][0] == 0:
            self.mainui.lblNotifNumber.setVisible(False)
        else:
            self.mainui.lblNotifNumber.setVisible(True)
        
        self.mainui.lblNotifNumber.setText(f"{sqlcountnotif()[0][0]}")
        self.mainui.lblNotifNumber.setStyleSheet('background-color: red; color: white; border-radius: 10px;')
        self.mainui.lblNotifNumber.setFixedSize(24, 24)
        
        #self.mainui.lblNotifNumber.move(self.mainui.pbNotification.width() - 5, 15)  # Adjust the position as needed

        #self.mainui.fmeIcon.setFixedSize(self.mainui.pbNotification.size())

    def showmenu(self):
        self.restartserialread()
        if self.burgermenuvisible == False:
            self.mainui.fmeMenu.show()
            self.burgermenuvisible = True
        else:
            self.mainui.fmeMenu.hide()
            self.burgermenuvisible = False
            

    def outlet1func(self):
        prevdataoutlet1 = len(self.outlet1) - 1
        # print("yoyo: ", self.outlet1[prevdataoutlet1])
        if self.mainui.chkKitchen.isChecked():
            #print("The Input Wattage of loads in ktchen is: ", self.outlet1)
            self.outlet_one = self.plot_widget.plot(self.outlet1, pen='r', symbol='o', symbolSize=1, symbolBrush='r')
        else:
            self.plot_widget.removeItem(self.outlet_one)
        return prevdataoutlet1

    def outlet2func(self):
        prevdataoutlet2 = len(self.outlet2) - 1
        # print("yoyo: ", self.outlet2[prevdataoutlet2])
        if self.mainui.chkBed.isChecked():
            #print("The Input Wattage of loads in bed is: ", self.outlet2)
            self.outlet_two = self.plot_widget.plot(self.outlet2, pen='g', symbol='o', symbolSize=1, symbolBrush='r')
        else:
            self.plot_widget.removeItem(self.outlet_two)

        return prevdataoutlet2

    def outlet3func(self):
        prevdataoutlet3 = len(self.outlet3) - 1
        # print("yoyo: ", self.outlet2[prevdataoutlet2])
        if self.mainui.chkLiving.isChecked():
            #print("The Input Wattage of loads living is: ", self.outlet3)
            self.outlet_three = self.plot_widget.plot(self.outlet3, pen='y', symbol='o', symbolSize=1, symbolBrush='r')
        else:
            self.plot_widget.removeItem(self.outlet_three)

        return prevdataoutlet3
    
    def limiterfunc(self):
        limiter = len(self.limiter) - 1

        self.limit = self.plot_widget.plot(self.limiter, pen='w', symbol='o', symbolSize=0, symbolBrush='r')
        
        return limiter

    def totalamperemeasurefunc(self):
        totamp = len(self.totalampere) - 1

        self.totalamp = self.plot_widget.plot(self.totalampere, pen='w', symbol='o', symbolSize=0, symbolBrush='r')
        
        return totamp

    def plot_chart(self):
        twentyfourhour = ""

        if self.mainui.chkKitchen.isChecked() or \
           self.mainui.chkBed.isChecked() or \
           self.mainui.chkLiving.isChecked():
            self.plot_widget.clear()

        self.outlet1.append(self.roomoutlet.kitchen_room_outlet())
        self.outlet1func()

        self.outlet2.append(self.roomoutlet.bed_room_outlet())
        self.outlet2func()

        self.outlet3.append(self.roomoutlet.living_room_outlet())
        self.outlet3func()
        
        self.limiter.append(sqlreadallwattage()[2]) #self.ac_apparent_power[0] * 0.80)
        self.limiterfunc()
        
        self.totalampere.append(sum([self.roomoutlet.kitchen_room_outlet(), \
                                     self.roomoutlet.bed_room_outlet(), \
                                     self.roomoutlet.living_room_outlet()]))
        self.totalamperemeasurefunc()

        twentyfourhour = datetime.now().strftime("%I:%M:%S %p")

        #print("this is time: ", twentyfourhour)
        acwattagedata(self, twentyfourhour)
        getamperage(self)
        getvoltage(self)
        
        self.kilo_watts_per_hour_meter()
        self.displaylabeltotalconsumption()

        if len(self.outlet1) == 35:
            self.outlet1 = [self.outlet1[self.outlet1func()]]
            self.outlet2 = [self.outlet2[self.outlet2func()]]
            self.outlet3 = [self.outlet3[self.outlet3func()]]
            self.limiter = [self.limiter[self.limiterfunc()]] #self.ac_apparent_power[0] * 0.80]
            self.totalampere = [self.totalampere[self.totalamperemeasurefunc()]]
            self.plot_widget.clear()
        
        self.notificationdesign()
        self.notificationupdatelist()
        
        #print("1:", round(self.outlet1[len(self.outlet1) - 1]))
        #print("2:", round(self.outlet2[len(self.outlet2) - 1]))
        #print("3:", round(self.outlet3[len(self.outlet3) - 1]))

if __name__ == '__main__':
    app = QApplication([])
    app.setOverrideCursor(Qt.CursorShape.BlankCursor)
    main = Graph()
    app.exec()
